CC := gcc
LD := gcc
LDFLAGS := 
LDLIBS := -lc
CFLAGS := -g 

APP_NAME := cpu-simulator

BUILD := ./build/
SRC := ./src/
C_SRC := ${shell find $(SRC) -name *.c}
C_OBJ := ${C_SRC:.c=.o}

.PHONY: clean run debug

$(BUILD)/$(APP_NAME) : $(C_OBJ)
	$(LD) -o $@ $^ $(LDLIBS)

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

run: $(BUILD)/$(APP_NAME)
	$(BUILD)/$(APP_NAME)

debug: $(BUILD)/$(APP_NAME)
	gdb $(BUILD)/$(APP_NAME)

clean:
	-rm -rf $(BUILD)/*
	-find $(SRC)/ -name *.o -delete
