#include "ram.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

static uint8_t previous_display_values[8] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};

struct RAM new_ram(uint32_t size) {
    struct RAM mem;
    mem.ram = malloc(size);
    mem.size = size;
    printf("size=%d\n", mem.size);
    return mem;
}

uint8_t load_byte(struct RAM ram, uint32_t index) {
    return *(ram.ram+index*sizeof(uint8_t));
}

void write_byte(struct RAM ram, uint32_t index, uint8_t byte) {
    *(ram.ram+index*sizeof(uint8_t)) = byte;
}

void display(struct RAM ram) {
    // printf("\t%d\n", previous_display_values[0]);
    assert(ram.size > 0);
    bool same_values = true;
    int display_start = ram.size-8;
    int display_end = ram.size;
    for (int i=display_start; i<display_end; i++) {
	if (previous_display_values[i-display_start] != ram.ram[i]) {
	    same_values = false;
	    break;
	}
    }
    if (!same_values) {
	for (int i=display_start; i<display_end; i++) {
	    printf("%u ", ram.ram[i]);
	    previous_display_values[i-display_start] = ram.ram[i];
	}
	printf("\n");
    }
}

void free_ram(struct RAM ram) {
    free(ram.ram);
}
