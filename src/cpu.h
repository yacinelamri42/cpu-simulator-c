#ifndef CPU__H
#define CPU__H

#include "common.h"
#include "ram.h"

struct CPU {
    uint8_t registers[4];
    uint8_t ip;
    uint8_t sp;
    uint8_t bp;
    uint8_t flags;
    struct RAM memory;
};

struct CPU new_cpu(uint8_t ram_size);
void run(struct CPU cpu);
#endif
