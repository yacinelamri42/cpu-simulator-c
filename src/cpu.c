#include "cpu.h"
#include "ram.h"
#include "opcode.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct CPU new_cpu(uint8_t ram_size) {
    struct CPU cpu;
    cpu.registers[0] = 0;
    cpu.registers[1] = 0;
    cpu.registers[2] = 0;
    cpu.registers[3] = 0;
    cpu.ip = 16;
    cpu.sp = 0;
    cpu.bp = 0;
    cpu.flags = 0;
    cpu.memory = new_ram(ram_size);
    return cpu;
}

struct Instruction {
    uint8_t opcode : 4;
    uint8_t rA : 2;
    uint8_t rB : 2;
};

void run(struct CPU cpu){
    for(; cpu.ip < cpu.memory.size; cpu.ip++){
	display(cpu.memory);
	uint8_t byte = load_byte(cpu.memory, cpu.ip);
	struct Instruction instruction = {
	    .opcode = (byte >> 4) & 0xf,
	    .rA = (byte >> 2) & 0b11,
	    .rB = byte & 0b11,
	};
	switch (instruction.opcode) {
	    case OP_MOVE:
		cpu.registers[instruction.rA] = cpu.registers[instruction.rB];
	    break;
	    case OP_ADD:
		cpu.registers[instruction.rA] += cpu.registers[instruction.rB];
	    break;
	    case OP_SUB:
		cpu.registers[instruction.rA] -= cpu.registers[instruction.rB];
	    break;
	    case OP_MUL:
		cpu.registers[instruction.rA] *= cpu.registers[instruction.rB];
	    break;
	    case OP_DIV:
		if (cpu.registers[instruction.rB] == 0) {
		    cpu.flags = 1;
		}else{
		    cpu.registers[instruction.rA] /= cpu.registers[instruction.rB];
		}
	    break;
	    case OP_SHIFTR:
		cpu.registers[instruction.rA] >>= cpu.registers[instruction.rB];
	    break;
	    case OP_SHIFTL:
		cpu.registers[instruction.rA] <<= cpu.registers[instruction.rB];
	    break;
	    case OP_AND:
		cpu.registers[instruction.rA] &= cpu.registers[instruction.rB];
	    break;
	    case OP_OR:
		cpu.registers[instruction.rA] |= cpu.registers[instruction.rB];
	    break;
	    case OP_XOR:
		cpu.registers[instruction.rA] ^= cpu.registers[instruction.rB];
	    break;
	    case OP_NOT:
		cpu.registers[instruction.rA] = ~cpu.registers[instruction.rA];
	    break;
	    case OP_LOADF:
		cpu.registers[instruction.rB] = load_byte(cpu.memory, cpu.registers[instruction.rA]);
	    break;
	    case OP_LOADT:
		write_byte(cpu.memory, cpu.registers[instruction.rA], cpu.registers[instruction.rB]);
	    break;
	    case OP_JMP:
		if (cpu.registers[instruction.rA] > 0 && cpu.registers[instruction.rA] < cpu.memory.size) {
		    cpu.ip = cpu.registers[instruction.rA];
		}else{
		    cpu.flags = 1<<1;
		}
	    break;
	    case OP_INCR:
		if (cpu.registers[instruction.rA] == 0xff) {
		    cpu.flags = 1<<2;
		}
		cpu.registers[instruction.rA] += 1;
	    break;
	    case OP_SEE_FLAG:
		cpu.registers[instruction.rA] = cpu.flags;
	    break;
	    default:
		fprintf(stderr, "Instruction %x not found\n", instruction.opcode);
		exit(1);
	    break;
	}
    }
}
