#ifndef RAM__H
#define RAM__H

#include "common.h"
#include <stdint.h>

struct RAM {
    uint8_t * ram;
    uint32_t size;
};

struct RAM new_ram(uint32_t size); 
uint8_t load_byte(struct RAM ram, uint32_t index);
void write_byte(struct RAM ram, uint32_t index, uint8_t byte);
void free_ram(struct RAM ram);
void display(struct RAM ram);

#endif
