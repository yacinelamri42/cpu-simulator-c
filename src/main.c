#include <stdint.h>
#include "cpu.h"
#include "ram.h"
#include "opcode.h"

void load_program(struct CPU cpu);

int main(int argc, char **argv){
    struct CPU cpu = new_cpu((uint8_t)255);
    load_program(cpu);
    run(cpu);
    free_ram(cpu.memory);
}

void load_program(struct CPU cpu) {
    write_byte(cpu.memory, 16, CREATE_INSTRUCTION(OP_INCR, 0, 0)); // increment register 0 by 1
    write_byte(cpu.memory, 17, CREATE_INSTRUCTION(OP_INCR, 0, 0)); // increment register 0 by 1
    write_byte(cpu.memory, 18, CREATE_INSTRUCTION(OP_INCR, 0, 0)); // increment register 0 by 1
    write_byte(cpu.memory, 19, CREATE_INSTRUCTION(OP_INCR, 1, 0)); // increment register 1 by 1
    write_byte(cpu.memory, 20, CREATE_INSTRUCTION(OP_SHIFTL, 1, 0)); // shift the value of register 1 by 3
    write_byte(cpu.memory, 21, CREATE_INSTRUCTION(OP_NOT, 1, 0)); // flip the value of register 1
    write_byte(cpu.memory, 22, CREATE_INSTRUCTION(OP_LOADT, 1, 0)); // Load value 3 to address in register 1
    write_byte(cpu.memory, 23, CREATE_INSTRUCTION(OP_NOT, 2, 0)); // get 0xff in register 2
    write_byte(cpu.memory, 24, CREATE_INSTRUCTION(OP_JMP, 2, 0)); // jump there to stop the program
}
