#ifndef OPCODE__H
#define OPCODE__H

#define OP_MOVE		0x0	/*RA = RB*/
#define OP_ADD 		0x1	/*RA += RB*/
#define OP_SUB 		0x2	/*RA -= RB*/
#define OP_MUL 		0x3	/*RA *= RB*/
#define OP_DIV 		0x4	/*RA /= RB*/
#define OP_SHIFTR 	0x5	/*RA <<= RB*/
#define OP_SHIFTL 	0x6	/*RA >>= RB*/
#define OP_AND 		0x7	/*RA &= RB*/
#define OP_OR 		0x8	/*RA |= RB*/
#define OP_XOR 		0x9	/*RA ^= RB*/
#define OP_NOT		0xA	/*RA = ~RA*/
#define OP_LOADF 	0xB 	/*Load from memory address in register A to register B*/
#define OP_LOADT 	0xC 	/*Load value in register B to memory address in register A*/
#define OP_JMP		0xD	/*Jump to address in register A*/
#define OP_INCR		0xE	/*Increment register A by 1*/
#define OP_SEE_FLAG	0xf	/* Load flag to RA*/


#define CREATE_INSTRUCTION(opcode, rA, rB) opcode << 4 | rA << 2 | rB

#endif
